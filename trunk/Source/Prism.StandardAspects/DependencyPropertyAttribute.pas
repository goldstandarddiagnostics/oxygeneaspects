﻿// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// This aspect can be applied on properties of a WPF control (form, Button, ...)
// It will modify a regular propery to a Dependency Property
namespace Prism.StandardAspects;

interface

uses
  System.Collections.Generic,
  System.Linq,
  System.Text,
  RemObjects.Oxygene.Cirrus,
  RemObjects.Oxygene.Cirrus.Values,
  RemObjects.Oxygene.Cirrus.Statements;

type

  [AttributeUsage(AttributeTargets.Property)]
  DependencyPropertyAttribute = public class(Attribute, IBaseAspect, IPropertyImplementationDecorator, IPropertyInterfaceDecorator)
  private
    fStaticFieldValue: FieldValue;
    fStaticMethod: IMethodDefinition;
    fUpdateMethod: IMethodDefinition;
  protected
  public
    method HandleInterface(Services: IServices; aProperty: IPropertyDefinition);
    method HandleImplementation(Services: IServices; aProperty: IPropertyDefinition; aRead: IMethodDefinition; aWrite: IMethodDefinition);
  end;

implementation

method DependencyPropertyAttribute.HandleImplementation(Services: IServices; aProperty: IPropertyDefinition; aRead: IMethodDefinition; aWrite: IMethodDefinition);
begin
  var selfVal := new SelfValue();
  var owner := aProperty.Owner;

  var retVal := new ResultValue;
  aRead.SetBody(Services, method
  begin
    var temp := unquote<System.Windows.DependencyObject>(selfVal).GetValue(unquote<System.Windows.DependencyProperty>(fStaticFieldValue));
    unquote(new AssignmentStatement(retVal, new UnaryValue(new NamedLocalValue('temp'), UnaryOperator.Cast, aRead.Result)));
  end);

  var firstParamValue := new ParamValue(0);
  aWrite.SetBody(Services, method
  begin
    unquote<System.Windows.DependencyObject>(selfVal).SetValue(unquote<System.Windows.DependencyProperty>(fStaticFieldValue), unquote<Object>(firstParamValue));
  end);

  var cons: IMethodDefinition;
  cons := IMethodDefinition(owner.GetClassConstructor());
  if not assigned(cons) then cons := owner.AddConstructor(true);
  var thisTypeValue := new TypeOfValue(owner.GetSelfType);
  var propType := new TypeOfValue(aProperty.Type);

  var methodExists := aProperty.Owner.GetMethods(fUpdateMethod.Name).Length > 0;

  if methodExists then
  begin
    cons.SetBody(Services, method
    begin
      unquote(fStaticFieldValue) := System.Windows.DependencyProperty.Register(unquote<System.String>(aProperty.Name),
                                                                               unquote<System.Type>(propType),
                                                                               unquote<System.Type>(thisTypeValue),
                                                                               new System.Windows.PropertyMetadata(unquote<System.Windows.PropertyChangedCallback>(new ProcPtrValue(new SelfValue(), fStaticMethod))));
      Aspects.OriginalBody;
    end);
  end
  else
  begin
    cons.SetBody(Services, method
    begin
      unquote(fStaticFieldValue) := System.Windows.DependencyProperty.Register(unquote<System.String>(aProperty.Name), unquote<System.Type>(propType), unquote<System.Type>(thisTypeValue));
      Aspects.OriginalBody;
    end);
  end;

  if methodExists then
  begin
    firstParamValue := new ParamValue(0);
    var secondParamValue := new ParamValue(1);
    fStaticMethod.SetBody(Services, method
    begin
      var temp := unquote<System.Windows.DependencyPropertyChangedEventArgs>(secondParamValue).NewValue;
      unquote(new ProcValue(new UnaryValue(firstParamValue, UnaryOperator.Cast, aProperty.Owner),
      fUpdateMethod,
      [new UnaryValue(new NamedLocalValue('temp'), UnaryOperator.Cast, aProperty.Type)]));
    end);
  end
  else
  begin
    aProperty.Owner.RemoveMethod(fStaticMethod);
  end;
end;

method DependencyPropertyAttribute.HandleInterface(Services: IServices; aProperty: IPropertyDefinition);
begin
  var DependencyObjectClass := Services.GetType('System.Windows.DependencyObject');
  if not aProperty.Owner.IsAssignableTo(DependencyObjectClass) then
  begin
    Services.EmitError('The DependencyProperty Attribute can only be used on properties of a System.Windows.DependencyObject or decendant class');
  end;
  var owner := aProperty.Owner;
  var dpt :=  Services.GetType('System.Windows.DependencyProperty');
  var staticField := owner.AddField(aProperty.Name + 'Property', dpt, true);
  staticField.Visibility := Visibility.Public;
  fStaticFieldValue := new FieldValue(new SelfValue(), staticField);
  fStaticMethod := owner.AddMethod(aProperty.Name + 'PropertyUpdater', nil, true);
  fStaticMethod.Visibility := Visibility.Private;
  fStaticMethod.AddParameter('sender', ParameterModifier.In, DependencyObjectClass);
  fStaticMethod.AddParameter('e', ParameterModifier.In, Services.GetType('System.Windows.DependencyPropertyChangedEventArgs'));
  fUpdateMethod := aProperty.WriteMethod;
  aProperty.WriteMethod := owner.AddMethod(aProperty.Name + 'DependencyPropertySetter', nil, false);
  aProperty.WriteMethod.AddParameter('value', ParameterModifier.In, aProperty.Type);
end;

end.